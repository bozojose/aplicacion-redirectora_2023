#!/usr/bin/python3

import socket
import random
import string

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind((socket.gethostbyname('localhost'), 1234))

mySocket.listen(15)

try:
    while True:
        print('Waiting for connections')
        (recvSocket, address) = mySocket.accept()
        print('Request received:')
        print(recvSocket.recv(2048))
        print('Answering back...')

        recurso_aleat = ''.join(random.choices(string.ascii_lowercase + string.digits, k=20))
        direccion = b"http://localhost:1234/" + bytes(str(recurso_aleat), 'utf-8')

        recvSocket.send(b"HTTP/1.1 302 Found\r\n\r\n" + b"<html><head>" +
                        b"<meta http-equiv='refresh' content='4; URL=" +
                        direccion + b"'></head><body><h2> Redirigiendo a " +
                        b"<a href=" + direccion + b"> http://localhost:1234/" +
                        bytes(str(recurso_aleat), 'utf-8') +
                        b"</a> ...</h2>" +
                        b"</body></html>" +
                        b"\r\n")

        recvSocket.close()

except KeyboardInterrupt:
    print("Closing binded socket")
    mySocket.close()

